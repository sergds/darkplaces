# DarkPlaces on Android
### ⚠️ Current state: Unmaintained unstable buggy and unusable trash. Use only if you want to see how xonotic runs on your device (if it will start or compile at all).
## For Xonotic (but it can be reconfigured for any game!) 
![scr1](screenshots/Screenshot_20210502-175403_Darkplaces.jpg) 

Copy gamedata to /sdcard/darkplaces/name_of_your_games_basedir (it would be /sdcard/darkplaces/data for xonotic).
Copy d0pk public key(s) (if any) to /sdcard/darkplaces 
### Things that work: 
- Sound/Net/Video 
- cURL (no ssl!) 
- Joystick Input 
- some very bad touch controls 
- Crypto (XonStat) 

### Things that do not work:

- Some Shaders (warpzones and reflections in xonotic, because framebuffer objects don't work on gles2) 
