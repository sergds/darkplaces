LOCAL_PATH := $(call my-dir)

###########################
#
# libpng static library
#
###########################

include $(CLEAR_VARS)

LOCAL_MODULE := png

LOCAL_C_INCLUDES := $(LOCAL_PATH)/

LOCAL_CFLAGS := \
	-DPNG_ARM_NEON_OPT=0

#ifeq ($(TARGET_ARCH),arm)
#    LOCAL_CFLAGS := \
#	-DPNG_ARM_NEON_OPT=1
#endif

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)

LOCAL_SRC_FILES := \
	$(LOCAL_PATH)/png.c \
	$(LOCAL_PATH)/pngerror.c \
	$(LOCAL_PATH)/pngget.c \
	$(LOCAL_PATH)/pngmem.c \
	$(LOCAL_PATH)/pngpread.c \
	$(LOCAL_PATH)/pngread.c \
	$(LOCAL_PATH)/pngrio.c \
	$(LOCAL_PATH)/pngrtran.c \
	$(LOCAL_PATH)/pngrutil.c \
	$(LOCAL_PATH)/pngset.c \
	$(LOCAL_PATH)/pngtrans.c \
	$(LOCAL_PATH)/pngwio.c \
	$(LOCAL_PATH)/pngwrite.c \
	$(LOCAL_PATH)/pngwtran.c \
	$(LOCAL_PATH)/pngwutil.c \
	#$(LOCAL_PATH)/arm/arm_init.c \
	#$(LOCAL_PATH)/arm/filter_neon_intrinsics.c \
	#$(LOCAL_PATH)/arm/filter_neon.S \
	#$(LOCAL_PATH)/arm/palette_neon_intrinsics.c \

#ifeq ($(TARGET_ARCH),arm)
#	LOCAL_SRC_FILES := \
#		$(LOCAL_PATH)/arm/arm_init.c \
#		$(LOCAL_PATH)/arm/filter_neon_intrinsics.c \
#		$(LOCAL_PATH)/arm/filter_neon.S \
#		$(LOCAL_PATH)/arm/palette_neon_intrinsics.c
#endif

LOCAL_LDLIBS := \
	-lz

include $(BUILD_SHARED_LIBRARY)
