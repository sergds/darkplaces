LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := z

LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/


LOCAL_CFLAGS := \
    -O2
# Add your application source files here...
# Note: this is the expansion of $(OBJ_CD) in Darkplaces's own Makefile.
LOCAL_SRC_FILES := \
    $(LOCAL_PATH)/adler32.c \
    $(LOCAL_PATH)/compress.c \
    $(LOCAL_PATH)/crc32.c \
    $(LOCAL_PATH)/deflate.c \
    $(LOCAL_PATH)/gzclose.c \
    $(LOCAL_PATH)/gzlib.c \
    $(LOCAL_PATH)/gzread.c \
    $(LOCAL_PATH)/gzwrite.c \
    $(LOCAL_PATH)/inflate.c \
    $(LOCAL_PATH)/infback.c \
    $(LOCAL_PATH)/inftrees.c \
    $(LOCAL_PATH)/inffast.c \
    $(LOCAL_PATH)/trees.c \
    $(LOCAL_PATH)/uncompr.c \
    $(LOCAL_PATH)/zutil.c \

LOCAL_LDLIBS := \
    -lm

include $(BUILD_SHARED_LIBRARY)
