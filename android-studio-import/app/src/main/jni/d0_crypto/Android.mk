LOCAL_PATH:= $(call my-dir)
LIBRARY_PATH:= $(LOCAL_PATH)/d0_blind_id

include $(CLEAR_VARS)
LOCAL_MODULE := d0_blind_id
LOCAL_CFLAGS := -DBYTE_ORDER=LITTLE_ENDIAN
LOCAL_C_INCLUDES += $(LIBRARY_PATH)/
LOCAL_SRC_FILES := \
	$(LIBRARY_PATH)/d0_blind_id.c \
	$(LIBRARY_PATH)/d0.c \
	$(LIBRARY_PATH)/d0_iobuf.c \
	$(LIBRARY_PATH)/sha2.c \
	$(LIBRARY_PATH)/d0_blind_id.h \
	$(LIBRARY_PATH)/d0.h \
	$(LIBRARY_PATH)/d0_iobuf.h \
	$(LIBRARY_PATH)/sha2.h \
	$(LIBRARY_PATH)/d0_bignum.h \
	$(LIBRARY_PATH)/d0_bignum-gmp.c
	
LOCAL_SHARED_LIBRARIES := \
	gmp

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := d0_rijndael
LOCAL_C_INCLUDES += $(LIBRARY_PATH)/
LOCAL_SRC_FILES := \
	$(LIBRARY_PATH)/d0_rijndael.c \
	$(LIBRARY_PATH)/d0_rijndael.h
	
LOCAL_SHARED_LIBRARIES := \
	gmp \
	d0_blind_id
include $(BUILD_SHARED_LIBRARY)