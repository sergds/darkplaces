LOCAL_PATH := $(call my-dir)

###########################
#
# libvorbis library
#
###########################

include $(CLEAR_VARS)

LOCAL_MODULE := vorbis

LIBOGG_PATH := $(LOCAL_PATH)/../ogg/libogg-1.3.2

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/libvorbis-1.3.5/include \
	$(LOCAL_PATH)/libvorbis-1.3.5/libs \
	$(LOCAL_PATH)/$(LIBOGG_PATH)/include

LOCAL_EXPORT_C_INCLUDES := \
	$(LOCAL_PATH)/include \

LOCAL_SRC_FILES := \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/mdct.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/smallft.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/block.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/envelope.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/window.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/lsp.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/lpc.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/analysis.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/synthesis.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/psy.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/info.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/floor1.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/floor0.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/res0.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/mapping0.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/registry.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/codebook.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/sharedbook.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/lookup.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/bitrate.c \
	$(LOCAL_PATH)/libvorbis-1.3.5/lib/vorbisfile.c

LOCAL_SHARED_LIBRARIES := \
	ogg

include $(BUILD_SHARED_LIBRARY)
