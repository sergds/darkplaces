package io.github.sergds.dp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceFragmentCompat

class OptionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        if (savedInstanceState == null) {
            //supportFragmentManager
            //        .beginTransaction()
            //        .replace(R.id.settings, SettingsFragment())
            //        .commit()
        }
        //supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }
    fun startDP(view: View){
        Log.w("LaunchOptions", "Starting SDLActivity")
        val intent = Intent(this, SDLActivity::class.java)
        startActivity(intent)
        return
    }
    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }
    }
}