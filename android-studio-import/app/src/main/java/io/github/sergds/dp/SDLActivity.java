package io.github.sergds.dp;

import android.Manifest;
import android.os.Environment;
import android.util.Log;

import androidx.core.app.ActivityCompat;

public class SDLActivity extends org.libsdl.app.SDLActivity {
    /**
     * This method is called by SDL before loading the native shared libraries.
     * It can be overridden to provide names of shared libraries to be loaded.
     * The default implementation returns the defaults. It never returns null.
     * An array returned by a new implementation must at least contain "SDL2".
     * Also keep in mind that the order the libraries are loaded may matter.
     * @return names of shared libraries to be loaded (e.g. "SDL2", "main").
     */
    protected String[] getLibraries() {
        return new String[] {
                "ogg",
                "vorbis",
                "z",
                "jpeg",
                "png",
                "hidapi",
                "SDL2",
                "gmp",
                "d0_blind_id",
                "d0_rijndael",
                "main"
        };
    }

    /**
     * This method is called by SDL before starting the native application thread.
     * It can be overridden to provide the arguments after the application name.
     * The default implementation returns an empty array. It never returns null.
     * @return arguments for the native application.
     */
    protected String[] getArguments() {
        Log.d("DPLauncher", "Requesting Storage Permission to access basedir/userdir.");
        ActivityCompat.requestPermissions(SDLActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        ActivityCompat.requestPermissions(SDLActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        ActivityCompat.requestPermissions(SDLActivity.this, new String[]{Manifest.permission.MANAGE_EXTERNAL_STORAGE},2);
        String[] Args = {
                "-xonotic",
                //"-readonly",
                //"-noconsole",
                //"+map _hudsetup",
                "-userdir", Environment.getExternalStorageDirectory().getPath() + "/darkplaces",
                "-basedir", Environment.getExternalStorageDirectory().getPath() + "/darkplaces",
                "+r_water 0",
                "+sv_threaded 1",
                //"+gl_vbo 0",
                //"+developer 0",
                "+cl_pitchspeed 350",
                "+cl_yawspeed 350",
                "+cl_forwardspeed 500",
                "+cl_sidespeed 500",
                "+vid_touchscreen 1",
                "+vid_touchscreen_outlinealpha 1"
        };
        Log.d("DPLauncher", "Starting Game With: " + Args.length + " args");
        return Args;
    }
}
